import boto3
import pytest
import upload_file
from moto import mock_s3

@mock_s3
def test_upload_file():
    conn = boto3.resource('s3', region_name='us-east-1')
    bucket='comp630-m1-f19'
    conn.create_bucket(Bucket=bucket)
    upload_file.upload_file('aat1006.boto', bucket, 'aat1006.boto')

    assert True
